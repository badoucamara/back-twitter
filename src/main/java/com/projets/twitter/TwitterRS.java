package com.projets.twitter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.twitter.api.GeoCode;
import org.springframework.social.twitter.api.SearchParameters;
import org.springframework.social.twitter.api.SearchParameters.ResultType;
import org.springframework.social.twitter.api.Trend;
import org.springframework.social.twitter.api.Tweet;
import org.springframework.social.twitter.api.Twitter;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.maps.DistanceMatrixApi;
import com.google.maps.DistanceMatrixApiRequest;
import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.errors.ApiException;
import com.google.maps.model.DistanceMatrix;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.LatLng;
import com.projets.twitter.model.Addresse;
import com.projets.twitter.model.TweetInfos;
import com.projets.twitter.model.Woeid;

@RestController
public class TwitterRS {
	static String googleKey = "AIzaSyChUYEhKW833sWyd_pA8LE4A8O17CkXv-0";

	static GeoApiContext context = new GeoApiContext.Builder().apiKey(googleKey).build();

	public static List<String> addresses = new ArrayList<>();
	public static List<String> markers = new ArrayList<>();

	Map<String, Long> woeIDs = Woeid.getWoeIDs();

	@Autowired
	private Twitter twitter;

	public Predicate<TweetInfos> isGeocodable = t -> {
		if (t.getLocation().length() > 3) {
			GeocodingResult[] code = null;
			try {
				code = GeocodingApi.geocode(context, t.getLocation()).await();
			} catch (ApiException | InterruptedException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (code.length > 0) {
				LatLng location = code[0].geometry.location;
				Addresse a = new Addresse(location, code[0].formattedAddress);
				t.setAddresse(a);
			}
			return true;

		}

		else {
			return false;

		}

	};

	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping("/getTweets/{hashtag}")
	public List<TweetInfos> findTweetsFromHashtag(@PathVariable("hashtag") String hashtag) throws IOException {

		List<Tweet> tweets = twitter.searchOperations()
				.search(new SearchParameters(hashtag).count(100).resultType(ResultType.RECENT)).getTweets();

		List<TweetInfos> tweetInfos = tweets.stream().flatMap(TweetInfos.TweetflatMapper).map(TweetInfos.ParseTweet)
				.filter(t -> t.getLocation().length() > 3).filter(isGeocodable).filter(t -> t.getAddresse() != null)
				.collect(Collectors.toList());
		return tweetInfos;
	}

	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping("/getTweets/coordinates/{latlng}")
	public List<TweetInfos> findTweetsFromCoordinates(@PathVariable("latlng") String latlng)
			throws ApiException, InterruptedException, IOException {

		String[] coordinates = latlng.split(",");
		double lat = Double.parseDouble(coordinates[0]);
		double lng = Double.parseDouble(coordinates[1]);
		LatLng coord = new LatLng(lat, lng);
		GeoCode geoCode = new GeoCode(coord.lat, coord.lng, 50);

		Predicate<TweetInfos> isNear = t -> {

			Boolean isClose = false;
			try {
				DistanceMatrixApiRequest req = DistanceMatrixApi.newRequest(context);
				DistanceMatrix result = req.origins(coord).destinations(t.getAddresse().getLocation()).await();
//				if(result.rows == null)
//					System.out.println("weird");	
				if (result.rows[0].elements.length > 0) {
					long distApart = result.rows[0].elements[0].distance.inMeters / 1000;
					if (distApart < 50)
						isClose = true;
				}
				
				

			} catch (ApiException | InterruptedException | IOException | NullPointerException e) {
				// TODO Auto-generated catch block
				System.out.println("No tweets around here");
				e.printStackTrace();
				
			}
			return isClose;

		};
		List<Tweet> tweets = twitter.searchOperations().search(new SearchParameters(" ").resultType(ResultType.RECENT).geoCode(geoCode).count(100))

				.getTweets();

		List<TweetInfos> tweetInfos = tweets.stream().flatMap(TweetInfos.TweetflatMapper).map(TweetInfos.ParseTweet)
				.filter(t -> !t.getText().startsWith("RT")).filter(t -> t.getLocation().length() > 3)
				.filter(isGeocodable).filter(t -> t.getAddresse() != null).filter(isNear).collect(Collectors.toList());

		return tweetInfos;

	}

	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping("/getTrends/{woeid}")
	public List<Trend> getTrends(@PathVariable("woeid") String woeid) {
		long id = woeIDs.get(woeid.toLowerCase());
		return twitter.searchOperations().getLocalTrends(id).getTrends();
	}

}
