package com.projets.twitter.model;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.LatLng;

public class Addresse {

	@Override
	public String toString() {
		return "Addresse [location=" + location + ", formattedAdress=" + formattedAdress + "]";
	}

	/**
	 * 
	 */
	private static final String googleKey = "AIzaSyChUYEhKW833sWyd_pA8LE4A8O17CkXv-0";

	private LatLng location;
	private String formattedAdress;

	
	public Addresse(LatLng location, String formattedAdress) {
		this.location = location;
		this.formattedAdress = formattedAdress;
	}

	public Addresse() {

	}

	public LatLng getLocation() {
		return location;
	}

	public void setLocation(LatLng location) {
		this.location = location;
	}

	public String getFormattedAdress() {
		return formattedAdress;
	}

	public void setFormattedAdress(String formattedAdress) {
		this.formattedAdress = formattedAdress;
	}

	public List<Addresse> getAdressFromList(List<String> addresses) throws Exception {
		File file = new File("data/addresses.json");
		FileWriter fw = new FileWriter(file);
		List<Addresse> results = new ArrayList<>();

		GeoApiContext context = new GeoApiContext.Builder().apiKey(googleKey).build();

		for (String address : addresses) {

			GeocodingResult[] code = GeocodingApi.geocode(context, address).await();

			LatLng location = code[0].geometry.location;

			Addresse a = new Addresse(location, code[0].formattedAddress);

			results.add(a);
		}

		Gson gson = new GsonBuilder().setPrettyPrinting().create();

		gson.toJson(results, fw);
		fw.flush();

		return results;
	}

	
	
	
	
}
