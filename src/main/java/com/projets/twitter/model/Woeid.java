package com.projets.twitter.model;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Woeid {
    private String name;
    private long woeid;

    public Woeid() {
    }

    public Woeid(String name, long woeid) {
        this.name = name;
        this.woeid = woeid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getWoeid() {
        return woeid;
    }

    public void setWoeid(long woeid) {
        this.woeid = woeid;
    }

    @Override
    public String toString() {
        return "Woeid{" +
                "name='" + name + '\'' +
                ", woeid='" + woeid + '\'' +
                '}';
    }
    public static Map<String, Long> getWoeIDs(){
        List<Woeid> woeids = new ArrayList<>();
        try {
            // create a reader
            Reader reader = Files.newBufferedReader(Paths.get("woeid.json"));
            // convert JSON array to list of users
            woeids = new Gson().fromJson(reader, new TypeToken<List<Woeid>>() {
            }.getType());

            // close reader
            reader.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        Map<String, Long> map = new HashMap<>();
        for (Woeid w:woeids) {
            map.put(w.name.toLowerCase(),w.woeid);
        }
        return map;
    }
}
