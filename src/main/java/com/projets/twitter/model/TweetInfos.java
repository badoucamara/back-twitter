package com.projets.twitter.model;

import org.springframework.social.twitter.api.Tweet;

import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.errors.ApiException;
import com.google.maps.model.GeocodingResult;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

public class TweetInfos implements Serializable {
	private static String googleKey = "AIzaSyChUYEhKW833sWyd_pA8LE4A8O17CkXv-0";
	static GeoApiContext context = new GeoApiContext.Builder().apiKey(googleKey).build();
	private static final long serialVersionUID = 1L;
	private String screenName;
	private Integer retweetCount;
	private Integer favoriteCount;
	private String location;
	private String text;
	private String url;
	private Date date;
	private Addresse addresse;
	private String photo;

	public TweetInfos(String screenName, Integer retweetCount, Integer favoriteCount, String location, String text,
			String url, Date date,String photo) {
		this.screenName = screenName;
		this.retweetCount = retweetCount;
		this.favoriteCount = favoriteCount;
		this.location = location;
		this.text = text;
		this.url = url;
		this.date = date;
		this.setPhoto(photo);
	}

	public TweetInfos() {
	}

	public static Function<Tweet, Stream<Tweet>> TweetflatMapper = tweet -> {
		List<Tweet> list = new ArrayList<>();
		list.add(tweet);
		if (tweet.isRetweet())
			list.add(tweet.getRetweetedStatus());
		return list.stream();
	};
	public static Function<Tweet, TweetInfos> ParseTweet = tweet -> new TweetInfos(tweet.getUser().getScreenName(),
			tweet.getFavoriteCount(), tweet.getRetweetCount(), tweet.getUser().getLocation(), tweet.getText(),
			(tweet.getEntities().getUrls().size() != 0) ? tweet.getEntities().getUrls().get(0).getExpandedUrl() : "",tweet.getCreatedAt(),tweet.getUser().getProfileImageUrl());

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public String getScreenName() {
		return screenName;
	}

	public Integer getRetweetCount() {
		return retweetCount;
	}

	public Integer getFavoriteCount() {
		return favoriteCount;
	}

	public String getLocation() {
		return location;
	}

	public String getText() {
		return text;
	}

	public String getUrl() {
		return url;
	}


	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Addresse getAddresse() {
		return addresse;
	}

	public void setAddresse(Addresse addresse) {
		this.addresse = addresse;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}




	@Override
	public String toString() {
		return "TweetInfos [screenName=" + screenName + ", retweetCount=" + retweetCount + ", favoriteCount="
				+ favoriteCount + ", location=" + location + ", text=" + text + ", url=" + url + ", date=" + date
				+ ", addresse=" + addresse + ", photo=" + photo + "]";
	}

	public static GeocodingResult[] geocode(String location) {

		context = new GeoApiContext.Builder().apiKey(googleKey).build();
		GeocodingResult[] code = null;
		try {
			code = GeocodingApi.geocode(context, location).await();
		} catch (ApiException | InterruptedException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return code.length > 0 ? code : null;

	}


}
